# -*- coding: utf-8 -*-
import Evaluate
import Models
import Split
import numpy


# import data
data = numpy.load(file='mnist_49.npz')

# class of the pattern
target = data['t']

# patterns
# attrbutes represent the brightness of the pixels of a 28x28 picture
attributes = data['x']

# Initialize NB model, to be used in folds
model = Models.GaussianNB()

accuracyTestSum = 0.0
accuracyTrainSum = 0.0

# Run for 10 folds
folds = 10
for fold in range(folds):
    # split dataset
    xtrain,xtest,ttrain,ttest = Split.splitRandom(attributes,target,0.25)
    # fit the model using train set
    model.fit(xtrain, ttrain)
    # Calculate Accuracy for test and train set
    accuracyTest = model.score(xtest, ttest)
    accuracyTrain = model.score(xtrain, ttrain)
    # Sum the acuracy of every fold
    accuracyTestSum += accuracyTest
    accuracyTrainSum += accuracyTrain
    
# Calculate and print the average accuracy
accuracyTestAverage = accuracyTestSum / folds
accuracyTrainAverage = accuracyTrainSum / folds
print('When training the GaussianNB model with the raw dataset')
print('Average Accuracy in Test Set:', accuracyTestAverage)
print('Average Accuracy in Train Set:', accuracyTrainAverage)
print('\n')
    

pca = Models.PCA()

acc_train=[]
acc_test=[]

# n_components tested
components = [1,2,5,10,20,30,40,50,100,200]

# for every n_components value
for fucksSake in components:
    # reset the sums, they will be recalculated for each componente
    accuracyTestSum = 0.0
    accuracyTrainSum = 0.0
    # set the number of components
    pca.setComponents(fucksSake)
    # transform the patterns to be described by their components
    x_pca = pca.transform(attributes)
    # split new dataset, which consists of components rather than attributes
    xtrain,xtest,ttrain,ttest = Split.splitRandom(x_pca,target,0.25)
    
    # Run for 10 folds
    folds = 10
    for fold in range(folds):
        # split dataset
        xtrain,xtest,ttrain,ttest = Split.splitRandom(attributes,target,0.25)
        # fit the model using train set
        model.fit(xtrain, ttrain)
        # Calculate Accuracy for test and train set
        accuracyTest = model.score(xtest, ttest)
        accuracyTrain = model.score(xtrain, ttrain)
        # Sum the acuracy of every fold
        accuracyTestSum += accuracyTest
        accuracyTrainSum += accuracyTrain
    # calculate average accuracy of folds
    accuracyTestAverage = accuracyTestSum / folds
    accuracyTrainAverage = accuracyTrainSum / folds
    # add the components count and coresponding accuracy as dict to the list
    acc_test.append({'components':fucksSake,'accuracy':accuracyTestAverage})
    acc_train.append({'components':fucksSake,'accuracy':accuracyTrainAverage})

print('When training the GaussianNB model with the PCA filtered dataset')
print('Average Accuracy in Test Set for each Component Count:')
print(acc_test)
print()
print('Average Accuracy in Train Set for each Component Count:')
print(acc_train)

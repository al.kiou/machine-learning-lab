#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon May 20 19:45:50 2019

@author: alkiou
"""

import numpy as np
from sklearn.svm import SVR
from pandas import read_csv
import sklearn.model_selection as sk
import sys

def regrevaluate(t, predict, criterion):
    
    value = 0
    if (criterion == 'mse'):
        for i in range(len(t)):
            value += ((1/len(t)) * (t[i] - predict[i])**2)
    elif(criterion == 'mae'): 
        for i in range(len(t)):
            value += (1/len(t)) * (t[i] - predict[i])
    return value


data = read_csv('./myhousing.data', header=None, error_bad_lines=False).values
numberOfPatterns, numberOfAttributes = np.shape(data)
gamma=[0.0001, 0.001, 0.01, 0.1]
C=[1, 10, 100, 1000]
value = 0


x = np.zeros((numberOfPatterns, numberOfAttributes-1))
t = np.zeros((1,506))
t = np.transpose(t)

for i in range(numberOfPatterns):
    x[i] = data[i,:13]
    t[i] = data[i,13]
maesum = msesum = 0
msemin = maemin = sys.float_info.max
gammaCminmse = {'gamma':0,'C':0}
gammaCminmae = {'gamma':0,'C':0}
   
for g in gamma:
    for c in C:
        xtrain, xtest, ttrain, ttest = sk.train_test_split(x, t, test_size = 0.2)
        network = SVR(C = c, kernel='rbf', gamma = g)
        network.fit(xtrain, ttrain)
        predictTest = network.predict(xtest)
        
        # Evaluation
        mse = regrevaluate(ttest,predictTest,'mse')
        mae = regrevaluate(ttest,predictTest,'mae')
        msesum += mse
        maesum += mae
        if (mse < msemin):
            msemin = mse
            gammaCminmse['gamma'] = g
            gammaCminmse['C'] = c
        if (mae < maemin):
            maemin = mae
            gammaCminmae['gamma'] = g
            gammaCminmae['C'] = c
        
#Show Results
averagemse = msesum / (len(gamma) * len(C))
averagemae = maesum / (len(gamma) * len(C))
print ('Average MSE: ' , averagemse)
print ('Minimum MSE: ' , msemin , ' with C and gamma values of ' , gammaCminmse['C'] , ' and ' , gammaCminmse['gamma'])
print ('Average MAE: ' , averagemae)
print ('Minimum MAE: ' , maemin , ' with C and gamma values of ' , gammaCminmae['C'] , ' and ' , gammaCminmae['gamma'])

# -*- coding: utf-8 -*-

import sklearn.model_selection as sk
import numpy as np

#splits the dataset to train and test at a given ratio. Basically it's sklear.train_test_split()
def splitRandom(attributes, target, ratio):
    xtrain,xtest,ttrain,ttest = sk.train_test_split(attributes,target,test_size=ratio)
    return xtrain,xtest,ttrain,ttest

#used for datasets with sorted by class datasets, and same item number per class
def splitSerial(attributes, target, numberOfClasses, itemsPerClass, ratio):
    trainSamples = itemsPerClass * (1-ratio)
    xtrain = []
    ttrain = []
    xtest = []
    ttest = []
    for i in numberOfClasses:
        #the first samples of the class are appended to the train set
        #while the last samples are appended to the test set
        xtrain = np.append(xtrain, attributes[itemsPerClass*i:itemsPerClass*i+trainSamples], axis=0)
        ttrain = np.append(ttrain, target[itemsPerClass*i:itemsPerClass*i+trainSamples], axis=0)
        xtest = np.append(xtest, attributes[itemsPerClass*i+trainSamples:itemsPerClass*(i+1)])
        ttest = np.append(ttest, target[itemsPerClass*i+trainSamples:itemsPerClass*(i+1)])
    return xtrain, xtest, ttrain, ttest


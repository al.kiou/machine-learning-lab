# -*- coding: utf-8 -*-
"""
Created on Sat May 18 20:31:36 2019

@author: mikep
"""

#libraries
import numpy as np
from pandas import read_csv
import sklearn.model_selection as sk
import sklearn.svm as sv
import matplotlib.pyplot as plt
import sklearn.neural_network as nn



def regrevaluate(t,predict,criterion):
    #these values are to find the current value of mse and mae
    global msevalue,maevalue
    
    #these are to help us find the avg mse and mae
    global mseall,maeall
    #depending on the criterion we find the value
    if criterion == 'mse':
        msevalue = 0
        mseall += round(msevalue,2) 
    elif criterion == 'mae':
        maevalue = 1
        maeall += round(maevalue,2)

#import data
data = read_csv('https://archive.ics.uci.edu/ml/machine-learning-databases/housing/housing.data', header=None).values
mseall = maeall = .0
#we put the data on an array
arr = np.shape(data)

#We set the number of patterns and attributes 
NumberOfPatterns,NumberOfAttributes = arr

#with var count we set the dimensions of an array full of zeroes 
count = (NumberOfAttributes-1)*NumberOfPatterns
x = np.zeros(count)

#we set an array full of zeroes with dimensions 1*NumberOfPatterns
t = np.zeros(1*NumberOfPatterns)

#x array has the first 13 attributes of all attributes
x = arr[:,0:13]

#t array has the 14nth attribute for each pattern 
t = arr[:,14]

low0c = low1c = 0
low0gamma = low1gamma = 0


#we use the loop for every possible value for both vars
for counter in range(4):
    x1 = input("Give an input for C: ")
    x2 = input("Give an input for gamma: ")
    count = 1
    
    #we use another loop for every fold
    for z in range(9):
        msevalue = maevalue = .0
        print("Fold " + str(count))
        
        #here we use the split method
        xtrain,xtest,ttrain,ttest = sk.train_test_split(x,t,test_size=0.1)
        
        #we create a model using the below attributes
        model = sv.SVR(C=float(x1),kernel='rbf',gamma=float(x2))
        
        #train method
        model.fit(xtrain,ttrain)
        
        #predict method
        predict_test = model.predict(xtest)
        
        #we choose our criterion
        criterion = input("Write the criterion you want to use : 1.mse 2.mae ")
        
        regrevaluate(ttrain,predict_test,criterion)
        #if it's the first fold we arrange the lowest of everything
        if count == 1:
            mseavg = round(mseall/9,2)
            maeavg = round(maeall/9,2)
            mselow = mseavg
            maelow = maeavg
            low0c = x1
            low0gamma = x2
            low1c = x1
            lowgamma = x2
        count += 1
    #we find the avgs and we set them on an array    
    mseavg = round(mseall/9,2)
    maeavg = round(maeall/9,2)
    arraym = [mseavg,maeavg]
    
    #with these ifs we search for the lowest averages and the combinations of c and gamma we achive them
    if mseavg < mselow:
        low0c = x1
        low0gamma = x2
        mselow = mseavg
    
    if maeavg < maelow:
        low1c = x1
        low1gamma = x2
        maelow = maeavg

print("The lowest mse avg is = " + mselow)
print("With c = " + low0c + " and gamma = " + low0gamma)
        
print("The lowest mae avg is = " + maelow)
print("With c = " + low1c + " and gamma = " + low1gamma)        

xtrain,xtest,ttrain,ttest = sk.train_test_split(x,t,test_size=0.1)

model = sv.SVR(C=float(low0c),kernel='rbf',gamma=float(low0gamma))
model.fit(xtrain,ttrain)
predict_test = model.predict(xtest)

plt.subplot()
plt.plot(ttest,'b|')
plt.plot(predict_test,'r.')
plt.show()

N = [5,10,20,13,40,50]       

count1 = 1        
for v in range(len(N)):
    print("Other fold " + count1 )
    model = nn.MLPRegressor()
    model.fit(xtrain,ttrain)
    predict_test = model.predict(xtest)
    
       
        
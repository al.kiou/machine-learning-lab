# -*- coding: utf-8 -*-
import Evaluate
import Models
import Split
import numpy
from pandas import read_csv


#insert csv data in the object
data = read_csv('http://archive.ics.uci.edu/ml/machine-learning-databases/iris/iris.data', header=None).values

#shape returns two values, giving the dimensions of the "data" array
numberOfPatterns,numberOfAttributes = numpy.shape(data)

#isolates the attributes of the plants
attributes = data[:,0:4]
#translate attributes object to float array!!!!!
attributes = attributes.astype(float)
#array of zeroes, this will be mapped later
target = numpy.zeros((numberOfPatterns))

#the dictionaries isolate one class
choice = 2 #input("What class to isolate ")
if choice == 1:
    map_dict = {"Iris-setosa": 1,
            "Iris-versicolor": 0,
            "Iris-virginica": 0}
elif choice == 2:
    map_dict = {"Iris-setosa": 0,
            "Iris-versicolor": 1,
            "Iris-virginica": 0}
elif choice == 3:
    map_dict = {"Iris-setosa": 0,
            "Iris-versicolor": 0,
            "Iris-virginica": 1}

#fill the target array with 0/1 according to their class
for i, size in enumerate(data):
    target[i] = map_dict.get(data[i,4])

folds=16
results = Evaluate.multipleResults(folds)
model = Models.SVC(C=100, gamma=0.3)
for fold in range(folds):    
    xtrain,xtest,ttrain,ttest = Split.splitRandom(attributes, target, 0.15)
    model.fit(xtrain, ttrain)
    output = model.predict(xtest)
    results.addResults(ttest, output)
    results.addSplitPlot(xtrain, xtest)
results.showResults()    

svc = sksvm.SVC(C=100000, gamma=0.0001)
svc.fit(attributes, target)
output = svc.predict(attributes)
Evaluate.evaluate(target, output)
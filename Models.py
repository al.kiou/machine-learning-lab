# -*- coding: utf-8 -*-
import sklearn.svm as sksvm
import sklearn.neural_network as sknn
import sklearn.naive_bayes as sknb
import sklearn.decomposition as skd
import numpy #as np


class LinearClassifier:
    def calculateWeights(attributes, target):
        #PseudoInverse of attributes array
        attributespinv = numpy.linalg.pinv((attributes))
        
        #calculate the weights
        weights = numpy.matmul(attributespinv, target)
        return weights
    
    def calculateOutput(attributes, weights):
        #ytrain is the output, and is mapped as 0/1
        output = numpy.matmul(attributes, weights)
        output [output < 0] = 0
        output [output > 0] = 1
        return output
#END OF LC


class Perceptron:
    weights = None
    
    # The training algorithm of perceptron, it is called to train the weights of the model
    def fit(self, attributes, target, epochs, beta):
        numberOfPatterns,numberOfAttributes = numpy.shape(attributes) # calculate shape of attributes to use in loops
        
        attributes = numpy.divide(attributes, numpy.linalg.norm(attributes)) # normalize the attributes matrix
        
        weights = numpy.random.randn(numberOfAttributes) # assign random weights to the synapses
        
        # perceptron algorithm
        for epoch in range(epochs):
            for pattern in range(numberOfPatterns):
                
                u = numpy.matmul(attributes[pattern,:], weights) # Calculate Sum
                
                # Activation Function: Step
                if (u < 0):
                    output = 0
                else:
                    output = 1
                
                #if prediction is false the the weights are recalculated
                if (target[pattern] != output):
                    weights = weights + beta * (target[pattern] - output) * attributes[pattern,:]
                    
        self.weights = weights
    
    # Train weights and then return the weights matrix, calls the "fit" function
    def trainWeights(self, attributes, target, epochs, beta):
        self.fit(attributes, target, epochs, beta)
        return self.weights
    
    # Outputs the class prediction for the given data set
    def predict(self, attributes):
        output = numpy.zeros(len(attributes)) # create the output array
        for pattern in range(len(attributes)): # for the number of patterns
      
            u = numpy.matmul(attributes[pattern,:], self.weights) # calculate the Sum on the neuron
            
            # use step function to output the class predicted 
            if (u < 0):
                output[pattern] = 0
            else:
                output[pattern] = 1
        return output

# Multi-Layer Perceptron
class MLP:
    model = None #instance of sknn.MLPClassifier()
    # Basic model parameters used to configure the model
    # visit the library's documentation to discover more parameters available
    trainAlgInput = None
    actFunInput = None # activation functiopn
    hiddenLayers = None # number of hidden layers
    maxEpochs = None # number of max training epochs
    
    def __init__(self):
        self.model = sknn.MLPClassifier()
    
    # Parameter configuration in code
    def configure(self, trainAlgInput, actFunInput, hiddenLayers, maxEpochs):
        self.trainAlgInput = trainAlgInput
        self.actFunInput = actFunInput
        self.hiddenLayers = hiddenLayers
        self.maxEpochs = maxEpochs
        self.model = sknn.MLPClassifier(hidden_layer_sizes=int(self.hiddenLayers), activation=self.actFunInput, solver=self.trainAlgInput, max_iter=int(self.maxEpochs))
    
    # Parameter configuration via Console    
    def configurePrompt(self):
        train_dict = { '1': 'sgd', '2': 'lbfgs', '3': 'adam'}
        
        print('Training Algorithm')
        print('1. (Stochastic Gradient Descent)')
        print('2. LBFGS (Limited memory Broyden–Fletcher–Goldfarb–Shanno')
        print('3. Adam')
        self.trainAlgInput = train_dict.get(input('Option: '))
        
        activation_dict = {'1': 'logistic', '2': 'tanh'}
        
        print('Hidden Layer Activation Function')
        print('1. logistic 0/1')
        print('2. tanh -1/1')
        self.actFunInput = activation_dict.get(input('Option '))
        
        self.hiddenLayers = input('Choose number of neurons: ')
        self.maxEpochs = input('Choose number of MAXEPOCHS: ')
        self.model = sknn.MLPClassifier(hidden_layer_sizes=int(self.hiddenLayers), activation=self.actFunInput, solver=self.trainAlgInput, max_iter=int(self.maxEpochs))
    
    # Calls the training function of MLP       
    def fit(self, attributes, target):
        self.model.fit(attributes, target)
     
    # classification of a data set
    # don't change the parameters between calling "fit" and "predict" functions
    def predict(self, attributes):    
        return self.model.predict(attributes) # classification output array 
        
    
# a brief implementation of SVC, using just C and gamma parameters
class SVC:
    model = None
    gamma = None
    C = None
    
    # Initialize model
    def __init__(self, gamma=0.01, C=10):
        self.model = sksvm.SVC(gamma=gamma, C=C)
    
    # configute 'C' and 'Gamma' values of model
    def configure(self, C, gamma):
        self.model = sksvm.SVC(gamma=gamma, C=C)    
    
    # fit model
    def fit(self, attributes, target):
        self.model.fit(attributes, target)
    
    # return the class predicion for the pattern given    
    def predict(self, attributes):
        return self.model.predict(attributes)

class GaussianNB:
    
    model = None
    
    # Initialize model
    def __init__(self):
        self.model = sknb.GaussianNB()
    
    # fit model
    def fit(self, attributes, target):
        self.model.fit(X = attributes, y = target)
    
    # return array of class predicion for the patterns given      
    def predict(self, attributes):
        return self.model.predict(X = attributes)
        
    # return calculated accuracy (float)    
    def score(self, attributes, target):
        return self.model.score(X = attributes, y = target)

class PCA:
    
    model = None
    components = None
    
    # Initialize model
    def __init__(self):
        self.model = skd.PCA()
        
    # Set the number of components, which will describe the patterns    
    def setComponents(self, components):
        self.model = skd.PCA(n_components = components)
        self.components = components
        
    # returns array, shape(n_attributes, n_components)
    # replaces attributes with components on every pattern
    def transform(self, attributes):
        return self.model.fit_transform(attributes)

    # retutns average log-likelihood of the samples
    def score(self, components):
        return self.model.score(components)

#! Variables Explaination
#data is the copy of the csv file
#attributes is first 4 columns of the data, which are the attributes of the plants
#target is the 5th column of data, with values 1 or 0 according to the map_dict
#map_dict seperates one class, giving the target the value of 1
#xtrain is the array of atributes used to train
#xtest is the array of attributes used for testing
#ttrain is the array of target values used to train
#ttest is the array of target values used for testing
#ttrain1 and ttest1 are just mapped to values -1/1 from 0/1

#import libraries
import numpy as np
from pandas import read_csv
import sklearn.model_selection as sk
import sklearn.neural_network as sknn
import matplotlib.pyplot as plt

#insert csv data in the object
data = read_csv('./myhousing.data', header=None).values

#shape returns two values, giving the dimensions of the "data" array
numberOfPatterns,numberOfAttributes = np.shape(data)

#isolates the attributes of the plants
attributes = data[:,0:4]
#translate attributes object to float array!!!!!
attributes = attributes.astype(float)

#create an array with 150 vertical "1"
#Stack it with the attributes array 
attributes = np.hstack((attributes, np.ones((150,1)) ))

#---we are going to plot the data in groups of different colors
#---acording to the plant type. The first and 3rd attribute will be ploted

#create a new plot
plt.subplot()
#plot the iris-setosa as blue asteriscs
plt.plot(attributes[0:50,0], attributes[0:50,2], "b*", label='Setosa')
#plot the iris-vergicolor as red dots
plt.plot(attributes[50:100,0], attributes[50:100,2], "r.", label='Versicolor')
#plot the iris-virginica as green large dots
plt.plot(attributes[100:150,0],attributes[100:150,2],"go", label='Virginica')
#give the plot a title
plt.title('Classes')
#self-explainatory
plt.legend(loc='upper left')
#print the plot
plt.show()
#this plot makes the difference in characteristics between the plants obvious

#array of zeroes, this will be mapped later
target = np.zeros((numberOfPatterns))

#the dictionaries isolate one plant type at a time
choice = 3 #eval(input("What class to isolate "))
if choice == 1:
    map_dict = {"Iris-setosa": 1,
            "Iris-versicolor": 0,
            "Iris-virginica": 0}
elif choice == 2:
    map_dict = {"Iris-setosa": 0,
            "Iris-versicolor": 1,
            "Iris-virginica": 0}
elif choice == 3:
    map_dict = {"Iris-setosa": 0,
            "Iris-versicolor": 0,
            "Iris-virginica": 1}

#a for loop to map the array
for i, size in enumerate(data):
    target[i] = map_dict.get(data[i,4])

#xtrain consists of the first 40 rows of atributes for every plant    
xtrain = np.append(attributes[0:40,:], attributes[50:90,:], axis=0)
xtrain = np.append(xtrain, attributes[100:140,:], axis=0)
#while xtest gets the other 10 for every plant
xtest = np.append(attributes[40:50,:], attributes[90:100,:], axis=0)
xtest = np.append(xtest, attributes[140:150,:], axis=0)
#same with the t array, the first 40 are for training
ttrain = np.append(target[0:40], target[50:90], axis=0)
ttrain = np.append(ttrain, target[100:140], axis=0)
#and the other 10 for testing
ttest = np.append(target[40:50], target[90:100], axis=0)
ttest = np.append(ttest, target[140:150], axis=0)

#plotting the train and test samples
plt.subplot()
plt.plot(xtrain[:,0], xtrain[:,2],"b.", label='train')
plt.plot(xtest[:,0], xtest[:,2],"ro", label='test')
plt.title('Train-Test Samples')
plt.legend(loc='upper left')
plt.show()
#the train and test samples are really close, that's what the machine learning
#algorithms will be trained to perceive


#Plotting 9 different splits in one figure
fig = plt.figure()
for z in range(9):
    xtrain,xtest,ttrain,ttest = sk.train_test_split(attributes,target,test_size=0.1)
    newplot = fig.add_subplot(3,3,z+1)
    newplot.plot(xtrain[:,0], xtrain[:,2], "b.", label='train')
    newplot.plot(xtest[:,0], xtest[:,2], "ro", label='test')
fig.legend(loc='upper left')
plt.show()

#create a clone of ttrain where values are -1/1, in contrast with 0/1
ttrain1 = np.copy(ttrain)
ttrain1 [ttrain1 == 0] = -1
#same with ttest
ttest1 = np.copy(ttest)
ttest1 [ttest1 == 0] = -1

#PseudoInverse of attributes array
xtrainpinv = np.linalg.pinv((xtrain))

#calculate the weights
weights =np.matmul(xtrainpinv, ttrain1)

#ytrain is the output, and is mapped as 0/1
ytrain = np.matmul(xtrain, weights)
ytrain [ytrain < 0] = 0
ytrain [ytrain > 0] = 1

#Test Set Output
ytest = np.matmul(xtest, weights)
ytest [ytest < 0] = 0
ytest [ytest > 0] = 1

#plot the predictions against the true values, Train Set
plt.subplot()
plt.plot(ttrain,"bo", label='train')
plt.plot(ytrain,"r.", label='prediction')
plt.title('Real vs Prediction, Train Test')
plt.legend(loc='upper left')
plt.show()

#plot the predictions against the true values, Test Set
plt.subplot()
plt.plot(ttest,"bo", label='train')
plt.plot(ytest,"r.", label='prediction')
plt.title('Real vs Prediction, Test Set')
plt.legend(loc='upper left')
plt.show()


#True/False Positive/Negative Calculation
tn = tp = fn = fp =0
for i in range(len(ttest)):
    for j in range(5):
        if(ytest[i] == -1 and ttest1[i] == -1):
            tn+=1
        elif(ytest[i] == -1 and ttest1[i] == 1 ):
            fn+=1
        elif(ytest[i] == 1 and ttest1[i] == 1):
            tp+=1
        elif(ytest[i] == 1 and ttest1[i] == -1):
            fp+=1


def evaluate( tp, tn, fp, fn):
    Accuracy = (tp + tn) / (tp + tn + fp + fn)
    print('Accuracy: ')
    print(Accuracy)
    
    Precision = tp / (tp + fp)
    print('Precision: ')
    print(Precision)
    
    Recall = tp / (tp + fn)
    print('Recall: ')
    print(Recall)
    
    Fmeasure = (Precision * Recall) / ((Precision + Recall)/2)
    print('Fmeasure: ')
    print(Fmeasure)
    
    Sensitivity = tp / (tp + fn)
    print('Sensitivity: ')
    print(Sensitivity)
    
    #Specificity = tn / (tn + fp)
    #print('Specificity: ')
    #print(Specificity)

evaluate( tp, tn, fp, fn)
    
def perceptron(xtrain, ttrain, MAXEPOCHS, beta):
    w = np.random.randn(len(xtrain[0])) # create random weights
    w = np.round(w, 2) # floating point down to 2
    u = np.zeros((len(xtrain[0]),5)) # Neuron stimulation 
    uSum = np.zeros((len(xtrain[0]))) # δημιουργια πινακα τελικης εξοδου νευρωνα
    counter = 0 # used for sublopts
    Correction = 0 
    y = np.zeros(len(xtrain[0])) # the neuron output values
    xtrain = np.divide(xtrain, np.linalg.norm(xtrain))
    w = np.divide(w, np.linalg.norm(w))
    
    # repeat for as many times as epochs given
    for i in range(MAXEPOCHS):
        counter += 1       
        
       
        for index in range(len(xtrain[0])): 
            u[index] = xtrain[index,:] * w[index]
            uSum[index] = np.sum(u[index])                   
            if(uSum[index] >= 0): 
                y[index] = 1 
            else: 
                y[index] = -1
            
            if(ttrain[index] != y[index]):
                w[index] = w[index] + ((beta * (ttrain[index] - y[index])) * np.sum(xtrain[index,:]))
                Correction += 1
                
        if(Correction == 0): break;
        elif(counter == MAXEPOCHS): break;
        # -------------------------------------------------------------
    print('Number of Epochs: ' + str(counter))  
    return np.divide(w, np.linalg.norm(w)) # επιστροφη των διορθωμενων βαρων

print("RESULTS WITH PERCEPTRON")

'''
weights = perceptron(xtrain, ttrain1, 4000, 0.051)

#ytrain is the output, and is mapped as 0/1
ytrain = np.matmul(xtrain, weights)
ytrain [ytrain < 0] = -1
ytrain [ytrain >= 0] = 1

#Test Set Output
ytest = np.matmul(xtest, weights)
ytest [ytest < 0] = -1
ytest [ytest >= 0] = 1

#calculate True/False Positive/Negative
tn = tp = fn = fp =0
for i in range(len(ttest)):
    for j in range(5):
        if(ytest[i] == -1 and ttest1[i] == -1):
            tn+=1
        elif(ytest[i] == -1 and ttest1[i] == 1 ):
            fn+=1
        elif(ytest[i] == 1 and ttest1[i] == 1):
            tp+=1
        elif(ytest[i] == 1 and ttest1[i] == -1):
            fp+=1





#plot the predictions against the true values, Train Set
plt.subplot()
plt.plot(ttrain1,"bo", label='train')
plt.plot(ytrain,"r.", label='prediction')
plt.title('Real vs Prediction, Train Test')
plt.legend(loc='upper left')
plt.show()

#plot the predictions against the true values, Test Set
plt.subplot()
plt.plot(ttest1,"bo", label='train')
plt.plot(ytest,"r.", label='prediction')
plt.title('Real vs Prediction, Test Set')
plt.legend(loc='upper left')
plt.show()

#evaluate results            
evaluate(tp, tn, fp, fn)
'''

train_dict = { '1': 'sgd',
              '2': 'lbfgs',
              '3': 'adam'}

print('Training Algoruthm')
print('1. (Stochastic Gradient Descent)')
print('2. LBFGS (Limited memory Broyden–Fletcher–Goldfarb–Shanno')
print('3. Adam')
trainAlgInput = train_dict.get(input('Option: '))

activation_dict = {'1': 'logistic',
                   '2': 'tanh'}

print('Hidden Layer Activation Function')
print('1. logistic 0/1')
print('2. tanh -1/1')
actFunInput = activation_dict.get(input('Option '))

numOfHiddenInput = input('Choose number of neurons: ')
numOfMaxEpochsInput = input('Choose number of MAXEPOCHS: ')

#sknn is the instance of the sklearn_Neural_Ntwork
model = sknn.MLPClassifier(hidden_layer_sizes=int(numOfHiddenInput), activation=actFunInput, solver=trainAlgInput, max_iter=int(numOfMaxEpochsInput))
#fit function trains the NN
model.fit(xtrain, ttrain1)
#predict function returns the results given by a set passed through the nn
ytrain = model.predict(xtrain) #train set output
ytest = model.predict(xtest) # test st output

#calculate True/False Positive/Negative
tn = tp = fn = fp =0
for i in range(len(ttest)):
    for j in range(5):
        if(ytest[i] == -1 and ttest1[i] == -1):
            tn+=1
        elif(ytest[i] == -1 and ttest1[i] == 1 ):
            fn+=1
        elif(ytest[i] == 1 and ttest1[i] == 1):
            tp+=1
        elif(ytest[i] == 1 and ttest1[i] == -1):
            fp+=1





#plot the predictions against the true values, Train Set
plt.subplot()
plt.plot(ttrain1,"bo", label='train')
plt.plot(ytrain,"r.", label='prediction')
plt.title('Real vs Prediction, Train Test')
plt.legend(loc='upper left')
plt.show()

#plot the predictions against the true values, Test Set
plt.subplot()
plt.plot(ttest1,"bo", label='train')
plt.plot(ytest,"r.", label='prediction')
plt.title('Real vs Prediction, Test Set')
plt.legend(loc='upper left')
plt.show()

#evaluate results            
evaluate(tp, tn, fp, fn)

#COMMENTS
#at model.fit() you need to use the ttrai or ttrain1 acording 
#to the hidden layer activation fuction as it either uses
#0/1 for logistic or -1/1 for tahn
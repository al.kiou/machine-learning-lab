"""
Evaluation File

Contains all the functions used for Plotting and Evaluating the outputs
of a trained model
"""
import matplotlib.pyplot as plt
import math

# calculate Mean Square/Average Error
def regrevaluate(target, predict):
    
    mse = mae = 0
    
    for i in range(len(target)):
        mse += ((1/len(target)) * (target[i] - predict[i])**2)
    for i in range(len(target)):
        mae += (1/len(target)) * (target[i] - predict[i])
    return mse,mae
    
# plots the true and predicted classes of the patterns
def plotTargetPredicted(target, predicted):
    
    plt.subplot()
    plt.plot(target,"bo", label='target')
    plt.plot(predicted,"r.", label='Prediction')
    plt.title('Target vs Prediction')
    plt.legend(loc='upper left')
    plt.show()
    return plt
    
# Evaluates the model predictions with the basic metrcis
# Accuracy, Precision, Recall, Fmeasure, Sensitivity, Specificity
# toPrint parameter defines if the results will be printed at the console
def evaluate(target, predicted, toPrint=True):
    
    if (len(target)!=len(predicted)):
        print ('Different Size Arrays')
    
    # First, the true/false positive/negatives are summed
    # Works for either 0/1 or -1/1 class enumeration
    tn = tp = fn = fp =0
    for i in range(len(target)):
        for j in range(5):
            if(predicted[i] <= 0 and target[i] <= 0):
                tn+=1
            elif(predicted[i] <= 0 and target[i] == 1 ):
                fn+=1
            elif(predicted[i] == 1 and target[i] == 1):
                tp+=1
            elif(predicted[i] == 1 and target[i] <= 0):
                fp+=1
    
    # Then, the metrics are calculated
    # if the divisor in a division is 0, the the metric is calculated also as 0
    try:
        Accuracy = (tp + tn) / (tp + tn + fp + fn)        
    except: 
        Accuracy = 0
    
    try:
        Precision = tp / (tp + fp)        
    except: 
        Precision = 0

    try:
        Recall = tp / (tp + fn)        
    except: 
        Recall = 0
    
    try:
        Fmeasure = (Precision * Recall) / ((Precision + Recall)/2)        
    except: 
        Fmeasure = 0

    try:
        Sensitivity = tp / (tp + fn)        
    except: 
        Sensitivity = 0

    try:
        Specificity = tn / (tn + fp)        
    except: 
        Specificity = 0
    
    #print the results to Console
    if (toPrint):
        print('Accuracy: ', Accuracy)
        print('Precision: ', Precision)
        print('Recall: ', Recall)
        print('Fmeasure: ', Fmeasure)
        print('Sensitivity: ', Sensitivity)
        print('Specificity: ', Specificity)
    
    return Accuracy, Precision, Recall, Fmeasure, Sensitivity, Specificity

# This is an object used to evaluate multiple predictions
class multipleResults:
    results = 0 # number of the total results expected
    counter = 0 # counter of current result added
    
    # sum of the metrics of every result
    accuracy = 0
    precision = 0
    recall = 0
    fmeasure = 0
    sensitivity = 0
    specificity = 0
    
    # figures for plotting
    figClasses = plt.figure() # plotting target-prediction class
    figSplit = plt.figure() # plotting split of patterns, drawed on 2D
    figureDimensions = 0 # height and length of the figure plots
    
    def __init__(self, results):
        self.results = results 
        # calculate figure dimensions
        # the height and length of figure are equal
        # and calculated by using the square root of reults count rounded to the upper integer
        root = math.sqrt(results)
        if (root % 1 > 0):
            self.figureDimensions = 1
        self.figureDimensions += root//1
        
    # add a new result    
    def addResults(self,target, predicted):
        
        self.counter += 1 # update counter 
        
        # create the new classes plot and add it to the figure
        newplot = self.figClasses.add_subplot(self.figureDimensions, self.figureDimensions, self.counter)
        newplot.plot(target,"bo", label='target')
        newplot.plot(predicted,"r.", label='Prediction')
        if(self.counter == 1):
            self.figClasses.legend(loc='upper left') # adds legend once on the figure
        
        # call the evaluation function and add the metrcis to their coresponding sum
        # they will be used at the end to calculate the mean value of each
        Accuracy, Precision, Recall, Fmeasure, Sensitivity, Specificity = evaluate(target, predicted, toPrint=False)
        self.accuracy += Accuracy
        self.precision += Precision
        self.recall += Recall
        self.fmeasure += Fmeasure
        self.sensitivity += Sensitivity
        self.specificity += Specificity
    
    # add the patters' train-test sets for plotting 
    def addSplitPlot(self,train, test, column1=0, column2=2):
        newplot = self.figSplit.add_subplot(self.figureDimensions, self.figureDimensions, self.counter)
        newplot.plot(train[:,column1], train[:,column2], "b.", label='train')
        newplot.plot(test[:,column1], test[:,column2], "ro", label='test') 
        if(self.counter == 1):
            self.figSplit.legend(loc='upper left')
    
    # print the mean value of the metrics to console    
    def showResults(self):
        print("Mean Accuracy: ", self.accuracy/self.counter)
        print("Mean Precision: ", self.precision/self.counter)
        print("Mean Recall: ", self.recall/self.counter)
        print("Mean Fmeasure: ", self.fmeasure/self.counter)
        print("Mean Sensitivity: ", self.sensitivity/self.counter)
        print("Mean Specificity: ", self.specificity/self.counter)
    
    
    
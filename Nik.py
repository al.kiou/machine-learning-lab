#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Apr  8 15:35:44 2019

@author: nikolai
"""

#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Apr  3 18:06:19 2019

@author: nikolai
"""

#-----------------------    INSTRUCTIONS -------------------------------

# ta 6 criterions dinontai me ti seira -> 0 -> 1 -> 2 -> 3 -> 4 -> 5
# xreiazetai pali arketi pliktrologisi................

#-----------------------------------------------------------------------



import numpy as np
from pandas import read_csv
import sklearn.model_selection as sk
import sklearn.neural_network as nn
import matplotlib.pyplot as plt

 # μεθοδος evaluate για ευρεση των criterions
def evaluate(ttest, predictTest, criterion):#!!!
    global tp, tn, fp, fn
    global Accuracy, Precision, Recall, Fmeasure, Sensitivity, Specificity
    global sum1, sum2, sum3, sum4, sum5, sum6
    global sumall1, sumall2, sumall3, sumall4, sumall5, sumall6
    tn = fn = tp = fp = .0 # true/false negatives/positives
    
    # τοσες επαναληψεις οσες και οι σειρες του ttest
    for i in range(len(ttest)):
        
        for j in range(5):
            
            if(predictTest[i] == -1 and ttest[i] == -1):
                tn+=1

            elif(predictTest[i] == -1 and ttest[i] == 1 ):
                fn+=1

            elif(predictTest[i] == 1 and ttest[i] == 1):
                tp+=1

            elif(predictTest[i] == 1 and ttest[i] == -1):
                fp+=1
    
    try:#διαχείρηση των περιπτώσεων διαίρεσης με το μηδέν
            
            crit_dict.get(criterion)()
            crit_dict.pop(criterion)
    
    except ZeroDivisionError:
        print("Division By Zero")
        crit_dict.pop(criterion)
        
    #----------------------------------------------------
#------------------------------------------------------------------------------

# παρακάτω είναι οι έξι μέθοδοι για τα accuracy, precision, recall, fmeasure, sensitivity και specificity, 
# και καλούνται παρακάτω για μέσω ενός dictionary 
# ενός dictionary
# σε αυτές τις μεθόδους πραγματοποιείται και συσσώρευση των στοιχείων ακρίβειας με σκοπό την εύρεση μέσου όρου
def accuracy():
    global Accuracy, sum1, sumall1
    Accuracy = (tp + tn) / (tp + tn + fp + fn)
    sum1 += Accuracy
    print('Accuracy: ')
    print(Accuracy)
    sumall1 += round(sum1,1)
                
def precision():
    global Precision, sum2, sumall2
    Precision = tp / (tp + fp)
    sum2 += Precision
    print('Precision: ')
    print(Precision)
    sumall2 += round(sum2,1)

def recall():
    global Recall, sum3, sumall3
    Recall = tp / (tp + fn)
    sum3 += Recall
    print('Recall: ')
    print(Recall)
    sumall3 += round(sum3,1)

def fmeasure():
    global Fmeasure, sum4, sumall4
    Fmeasure = (Precision * Recall) / ((Precision + Recall)/2)
    sum4 += Fmeasure
    print('Fmeasure: ')
    print(Fmeasure)
    sumall4 += round(sum4,1)

def sensitivity():
    global Sesitivity, sum5, sumall5
    Sensitivity = tp / (tp + fn)
    sum5 += Sensitivity
    print('Sensitivity: ')
    print(Sensitivity)
    sumall5 += round(sum5,1)

def specificity():
    global Specificity, sum6, sumall6
    Specificity = tn / (tn + fp)
    sum6 += Specificity
    print('Specificity: ')
    print(Specificity)
    sumall6 += round(sum6,1)
#------------------------------------------------------------------------------    
def createDictionary():   
    global crit_dict
    crit_dict =  {1: accuracy,
                 2: precision,
                 3: recall,
                 4: fmeasure,
                 5: sensitivity,
                 6: specificity
                 }
#!!!---------------------------------------------------------------------------
    
createDictionary()#αρχικοποιηση του dictionary
data = read_csv('http://archive.ics.uci.edu/ml/machine-learning-databases/iris/iris.data', header=None).values
numberOfPatterns, numberOfAttributes = np.shape(data)
x = data[:,0:4] # κρατάω μόνο τα attributes στον χ
sumall1 = sumall2 = sumall3 = sumall4 = sumall5 = sumall6 = .0 #προς χρηση της μεσης τιμης στο τελος


# μία εικόνα για το πού βρίσκεται η κάθε κλάση οπτικά
plt.subplot()
plt.plot(x[0:50,0], x[0:50,2], 'm.')
plt.plot(x[50:100,0], x[50:100,2], 'co')
plt.plot(x[100:150,0], x[100:150,2], 'k*')
plt.title('First differences')
plt.show()
print(' ')

# create an array with Ones *150 rows and *1 column
temp = np.ones((150,1))
x = np.hstack((x,temp)) #stack x and temp

t = np.zeros((numberOfPatterns)) # δημιουργία πίνακα με μηδενικά, διάσταση -> numberOfPatterns (150)

answer = 'yes' # αρχικοποίηση απαντήσεως

while (answer == 'yes'):
    print('Παρακαλώ δώστε αριθμό επιλογής!')
    print('1 Iris-setosa from Iris-versicolor, Iris-virginica')
    print('2 Iris-virginica from Iris-setosa, Iris-versicolor')
    print('3 Iris-versicolor from Iris-setosa, Iris-virginica')
    ch = input('Option: ')
#------------------------------------------------------------------------------
    
    def tFill(): # !!!μέθοδος για γέμισμα του t με την map_dict
        for index, pattern in enumerate(data):
            t[index] = map_dict.get(data[index,4])

    if (ch == '1'):
        map_dict = {"Iris-setosa": 1,
                 "Iris-versicolor": -1,
                 "Iris-virginica": -1}
        tFill()

    elif (ch == '2'):
        map_dict = {"Iris-setosa": -1,
                 "Iris-versicolor": -1,
                 "Iris-virginica":1}
        tFill()

    elif (ch == '3'):
        map_dict = {"Iris-setosa": -1,
                 "Iris-versicolor": 1,
                 "Iris-virginica":-1}
        tFill()

    else: answer == 'no'

#------------------------------------------------------------------------------
#-Διαμοιρασμός και εμφάνιση των πρώτων 40 και των τελευταίων 10 απο κάθε κλάση-

    xtrain = np.append(x[0:40,:], x[50:90,:], axis=0)
    xtrain1 = np.append(xtrain, x[100:140,:], axis=0)

    xtest = np.append(x[40:50,:], x[90:100,:], axis=0)
    xtest = np.append(xtest, x[140:150,:], axis=0)

    ttrain = np.append(t[0:40], t[50:90], axis=0)
    ttrain1 = np.append(ttrain, t[100:140], axis=0)

    ttest = np.append(t[40:50], t[90:100], axis=0)
    ttest = np.append(ttest, t[140:150], axis=0)

#------------------------------------------------------------------------------
    #μετατροπη πινακων σε float για διευκολυνση στην χρηση
    x = x.astype(float)
    xtest = xtest.astype(float)
    xtrain = xtrain.astype(float)

#------------------------------------------------------------------------------
    train_dict = { '1': 'sgd',
                  '2': 'lbfgs',
                  '3': 'adam'}
    
    print('Παρακαλώ δώστε αριθμό επιλογής αλγορίθμου εκπαίδευσης')
    print('1. Στοχαςτική κατάβαςη δυναμικοφ (Stochastic Gradient Descent)')
    print('2. LBFGS (Limited memory Broyden–Fletcher–Goldfarb–Shanno')
    print('3. Adam')
    trainAlgInput = train_dict.get(input('Option: '))
    
    activation_dict = {'1': 'logistic',
                       '2': 'tanh'}
    
    print('Παρακαλώ δώστε αριθμό επιλογής συνάρτησης ενεργοποίησης κρυφού στρώματος')
    print('1. ‘logistic’ – σιγμοειδής 0/1')
    print('2. ‘tanh’ – υπερβολική εφαπτομένη -1/1')
    actFunInput = activation_dict.get(input('Option '))

    numOfHiddenInput = input('Choose number of neurons: ')
    numOfMaxEpochsInput = input('Choose number of MAXEPOCHS: ')
    

    model = nn.MLPClassifier(hidden_layer_sizes=int(numOfHiddenInput), activation=actFunInput, solver=trainAlgInput, max_iter=int(numOfMaxEpochsInput))
    model.fit(xtrain, ttrain)
    predict_train = model.predict(xtrain)

    plt.subplot()
    plt.plot(ttrain, 'bo')
    plt.plot(predict_train, 'g*')
    plt.title('training and its predictions using a mlpClassifier')
    plt.show()
    print(' ')

    predict_test = model.predict(xtest)
    plt.subplot()
    plt.plot(ttest, 'bo')
    plt.plot(predict_test, 'g*')
    plt.title('testing and its predictions using a mlpClassifier')
    plt.show()
    print(' ')

    
#------------------------------------------------------------------------------
    #μετατροπη πινακων σε float για διευκολυνση στην χρηση
    x = x.astype(float)
    xtest = xtest.astype(float)
    xtrain = xtrain.astype(float)
    
    
    fig = plt.figure(figsize=(15,10)) # δημιουργία του γενικου container που τα εμπεριέχει και τα 9 folds
    count = 1 # μετρητής για τα subplots
    
    
    for times in range(9): # δημιουργία των 9 folds
        xtrain, xtest, ttrain, ttest = sk.train_test_split(x, t, test_size = 0.2) #εφαρμογη της train_test_split
        # ευρεση μηκους του xtrain και xtest προς μελλοντικη χρηση
        xtrainLen = len(xtrain)
        xtestLen = len(xtest)
        
        model.fit(xtrain, ttrain)
        predictTest = model.predict(xtest)
        
#------------------------------------------------------------------------------
            
        sum1 = sum2 = sum3 = sum4 = sum5 = sum6 = .0 # μηδενισμος των εκαστοτε αθροισματων για καθε criterion του αντιστοιχου fold
        
            
        # προσοχη, ακολουθει μπακαλικη νοοτροπια
        criterion = 1
        while (crit_dict != {}):
            evaluate(ttest, predictTest, criterion)  
            criterion += 1
        print('===================================================')
        print(' ')
        print('new plot')    
        createDictionary()
        # προσθεση του καινουργιου plot
        pltfig = fig.add_subplot(3, 3, count)
        pltfig.plot(ttest, 'mo')
        pltfig.plot(predictTest, 'c.')
        count += 1 # παμε στο επομενο grid του figure

    
    fig.subplots_adjust(hspace=.2,wspace=.2) # ρυθμίσεις εμφάνισης
    plt.show() # εμφανιση του τελικου figure με τα plots
    
    # τυπωση των μεσων τιμων για ολα τα folds ----------------------------
    print('Mean Accuracy: ' + str(round((sumall1/9),2)))
    print('Mean Precision: ' + str(round((sumall2/9),2)))
    print('Mean Recall: ' + str(round((sumall3/9),2)))                          
    print('Mean Fmeasure: ' + str(round((sumall4/9),2)))
    print('Mean Sensitivity: ' + str(round((sumall5/9),2)))
    print('Mean Specificity: ' + str(round((sumall6/9),2)))           
    sumall1 = sumall2 = sumall3 = sumall4 = sumall5 = sumall6 = .0
    createDictionary()                    
    criterion = 1
    answer = input("Want to continue? yes or no: ") # προτροπη προς τον χρήστη για συνέχιση του προγράμματος ή όχι
    
    
    